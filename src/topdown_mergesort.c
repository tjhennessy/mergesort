#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "topdown_mergesort.h"

//--------------------BEGIN STATIC FUNCTIONS-------------------------
static void goodbye()
{
    printf("Goodbye\n");
}

static void fill_rand_ints(int *a, int sz)
{
    for (int i = 0; i < sz; i++)
    {
        a[i] = rand() % sz;
    }
}

static void print(int *a, int sz)
{
    printf("[");
    for (int i = 0; i < sz; i++)
    {
        if ((i + 1) == sz)
        {
            printf("%d", *(a + i));
        }
        else
        {
            printf("%d, ", *(a + i));
        }
    }
    printf("]\n");
}

static bool is_sorted(int *a, cmp_f *less_than, int sz)
{
    for (int i = 1; i < sz; i++)
    {
        if (less_than(a[i], a[i - 1]))
        {
            return false;
        }
    }
    return true;
}

static bool less(const int key1, const int key2)
{
    return key1 < key2;
}

//---------------------END STATIC FUNCTIONS------------------------------

void sort(int *a, int *aux, cmp_f *compare, int lo, int hi)
{
    if (hi <= lo)
    {
        return;
    }

    int mid = lo + (hi - lo) / 2;
    sort(a, aux, compare, lo, mid);
    sort(a, aux, compare, mid + 1, hi);
    merge(a, aux, compare, lo, mid, hi);
}

void merge(int *a, int *aux, cmp_f *less_than, int lo, int mid, int hi)
{
    int left = lo;
    int right = mid + 1;

    for (int i = lo; i <= hi; i++)
    {
        aux[i] = a[i];
    }
    
    for (int i = lo; i <= hi; i++)
    {
        if (left > mid)
        {
            a[i] = aux[right++];
        }
        else if (right > hi)
        {
            a[i] = aux[left++];
        }
        else if (less_than(aux[left], aux[right]))
        {
            a[i] = aux[left++];
        }
        else
        {
            a[i] = aux[right++];
        }
    }
}

void mergesort(int *a, cmp_f *compare, int sz)
{
    int aux[sz];
    memset(aux, 0, sz * sizeof(int));
    sort(a, aux, compare, 0, sz - 1);
    assert(is_sorted(a, compare, sz));
}

int main(void)
{
    int arr_of_ints[ARR_SIZE] = {0};

    fill_rand_ints(arr_of_ints, ARR_SIZE);
    //print(arr_of_ints, ARR_SIZE);

    mergesort(arr_of_ints, less, ARR_SIZE);

    //print(arr_of_ints, ARR_SIZE);
    
    atexit(goodbye);

    return 0;
}