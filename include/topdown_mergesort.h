#ifndef _TOPDOWN_MERGESORT_H
#define _TOPDOWN_MERGETSORT_H

#define ARR_SIZE 1000000

typedef bool (cmp_f)(const int k1, const int k2);

void mergesort(int *a, cmp_f *compare, int sz);
void sort(int *a, int *aux, cmp_f *compare, int lo, int hi);
void merge(int *a, int *aux, cmp_f *less, int lo, int mid, int hi);

#endif /* _TOPDOWN_MERGESORT_H */